<?php
 return array (
  'table' => 'z_post',
  'title' => '文章',
  'description' => '文章',
  'auto_timestamp' => true,
  'button_default' => 
  array (
    0 => 'create',
    1 => 'update',
    2 => 'delete',
    3 => 'refresh',
  ),
  'page' => true,
  'extend' => 
  array (
  ),
  'pk' => 'id',
  'button' => 
  array (
  ),
  'fields' => 
  array (
    'id' => 
    array (
      'title' => 'id',
      'field' => 'id',
      'default' => '',
      'weight' => 1,
      'search_type' => 'input',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'hidden',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    ),
    'title' => 
    array (
      'title' => '标题',
      'field' => 'title',
      'default' => '',
      'weight' => 15,
      'search_type' => 'input',
      'search' => 'LIKE',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    ),
    'post_tags' => 
    array (
      'relation' => true,
      'field' => 'post_tags',
      'title' => '标签',
      'weight' => 17,
      'option_type' => 'option_remote_relation',
      'table_sort' => 0,
      'option_remote_relation' => 
      array (
        0 => 'z_post_tag',
        1 => 'id',
        2 => 'post_id',
        3 => 'tag_id',
        4 => 'z_tags',
        5 => 'id',
        6 => 'title',
      ),
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_extend' => 
      array (
      ),
      'form_type' => 'select',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
        'multiple' => 'true',
        'filterable' => 'true',
        'allow-create' => 'true',
        'default-first-option' => 'true',
      ),
      'save_format' => 
      array (
      ),
    ),
    'description' => 
    array (
      'title' => '描述',
      'field' => 'description',
      'default' => '',
      'weight' => 20,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => '_',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'input',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
        'type' => 'textarea',
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    ),
    'content' => 
    array (
      'title' => '内容',
      'field' => 'content',
      'default' => NULL,
      'weight' => 25,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => '_',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'editor',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'sort' => 
    array (
      'title' => '排序',
      'field' => 'sort',
      'default' => NULL,
      'weight' => 30,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'number',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'status' => 
    array (
      'title' => '状态',
      'field' => 'status',
      'default' => '1',
      'weight' => 35,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'radio',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_config',
      'option_config' => 
      array (
        0 => '草稿',
        1 => '显示',
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => '',
      ),
      'option_remote_relation' => '',
    ),
    'create_time' => 
    array (
      'title' => '发布时间',
      'field' => 'create_time',
      'default' => NULL,
      'weight' => 40,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => '_',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'update_time' => 
    array (
      'title' => '修改时间',
      'field' => 'update_time',
      'default' => NULL,
      'weight' => 45,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => '_',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => '_',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_default',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => '',
      'option_remote_relation' => '',
    ),
    'user_id' => 
    array (
      'title' => '发布者',
      'field' => 'user_id',
      'default' => '',
      'weight' => 50,
      'search_type' => '_',
      'search' => '=',
      'search_extend' => 
      array (
      ),
      'table_type' => 'column',
      'table_format' => 
      array (
      ),
      'table_sort' => false,
      'table_extend' => 
      array (
      ),
      'form_type' => 'take',
      'marker' => '',
      'form_format' => 
      array (
      ),
      'form_extend' => 
      array (
        'url' => '/curd/page/index?_table=z_user',
      ),
      'save_format' => 
      array (
      ),
      'relation' => false,
      'option_type' => 'option_relation',
      'option_config' => 
      array (
      ),
      'option_lang' => '',
      'option_relation' => 
      array (
        0 => 'z_user',
        1 => 'id',
        2 => 'username',
      ),
      'option_remote_relation' => '',
    ),
  ),
);