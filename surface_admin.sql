/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100323
 Source Host           : localhost:3306
 Source Schema         : surface_admin

 Target Server Type    : MySQL
 Target Server Version : 100323
 File Encoding         : 65001

 Date: 04/07/2021 11:08:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for z_admin
-- ----------------------------
DROP TABLE IF EXISTS `z_admin`;
CREATE TABLE `z_admin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户密码',
  `create_time` int(10) NOT NULL DEFAULT 0 COMMENT '注册时间',
  `status` enum('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '账号状态 0封号 1正常',
  `update_time` int(11) NOT NULL DEFAULT 0,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '头像',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_admin
-- ----------------------------
INSERT INTO `z_admin` VALUES (1, 'admin', 'admin', '$2y$10$UybQERZCiMdchq1JEOi6tObvKqa69L.O9IA9m6xf0J4iJC2jwUo56', 1562810827, '1', 1605865528, '');
INSERT INTO `z_admin` VALUES (2, 'lalala', '啦啦啦', '$2y$10$rVREDcmjzciyJ2.V.L7Vx.F4MNhTGeju6.vjtkN49AdnI0F6QpPQW', 1535080343, '1', 1624150540, '/uploads/20210619/bc7698478f5c416de6e26a96d497e09fa54ea08a.jpg');
INSERT INTO `z_admin` VALUES (3, 'zhangsan', '张二狗', '$2y$10$44GoYegX3jdQgXLFfpjcMulxmha9oSFVvPBj2qervmMNPaTEaxmm6', 1535080343, '1', 1624093833, '');

-- ----------------------------
-- Table structure for z_attachment
-- ----------------------------
DROP TABLE IF EXISTS `z_attachment`;
CREATE TABLE `z_attachment`  (
  `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uuid` char(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'filehash',
  `uploader` int(10) NOT NULL DEFAULT 0 COMMENT '上传者',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '物理路径',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小',
  `mime` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'mime类型',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建日期',
  `ip` int(10) NOT NULL DEFAULT 0 COMMENT '上传者IP',
  `width` int(11) NOT NULL COMMENT '宽度',
  `height` int(11) NOT NULL COMMENT '高度',
  `suffix` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片类型',
  `domain` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '域名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_attachment
-- ----------------------------
INSERT INTO `z_attachment` VALUES (5, 'bc7698478f5c416de6e26a96d497e09fa54ea08a', 0, '/uploads/20210619/bc7698478f5c416de6e26a96d497e09fa54ea08a.jpg', 'avatar.jpg', 1208, 'image/jpeg', 1624113532, 2130706433, 300, 300, 'jpg', '');
INSERT INTO `z_attachment` VALUES (6, '00884c1e8cbc36921e6ba83f35c2cb695e8fcdfa', 1, '/uploads/20210620/00884c1e8cbc36921e6ba83f35c2cb695e8fcdfa.jpg', 'no_goods.jpg', 13824, 'image/jpeg', 1624169413, 2130706433, 200, 200, 'jpg', '');
INSERT INTO `z_attachment` VALUES (7, 'b7552a703dd0d899d7886d124dafdb8e8dbd8d84', 1, '/uploads/20210620/b7552a703dd0d899d7886d124dafdb8e8dbd8d84.png', '1.png', 69209, 'image/png', 1624169922, 2130706433, 1016, 267, 'png', '');
INSERT INTO `z_attachment` VALUES (8, '39409ec1647fa726b2f5dd5f85258696a6dcf7b2', 1, '/uploads/20210704/39409ec1647fa726b2f5dd5f85258696a6dcf7b2.jpg', '9dc2a24610.jpg', 449849, 'image/jpeg', 1625367862, 2130706433, 800, 1162, 'jpg', '');

-- ----------------------------
-- Table structure for z_post
-- ----------------------------
DROP TABLE IF EXISTS `z_post`;
CREATE TABLE `z_post`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '描述',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `sort` smallint(4) NOT NULL COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(10) NOT NULL COMMENT '发布时间',
  `update_time` int(10) NOT NULL COMMENT '修改时间',
  `user_id` int(11) NOT NULL COMMENT '发布者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_post
-- ----------------------------
INSERT INTO `z_post` VALUES (7, '城市风光视频', '城市风光视频', '<p><iframe src=\"http://curd.demo.zsw.ink/static/backend/bg.mp4\"></iframe></p>', 0, 0, 1584282332, 1625368083, 1);
INSERT INTO `z_post` VALUES (8, '啦啦啦', '啦啦啦啦啦啦', '<p style=\"text-align:center;\">&nbsp;哈哈哈哈<br/></p>', 0, 0, 1584282332, 1625367976, 1);
INSERT INTO `z_post` VALUES (9, '可爱短发妹子俏皮', '可爱短发妹子俏皮', '<p><img src=\"/uploads/20210704/39409ec1647fa726b2f5dd5f85258696a6dcf7b2.jpg\" style=\"max-width:100%;\"/><br/></p>', 0, 0, 1584282332, 1625367863, 1);
INSERT INTO `z_post` VALUES (18, 'VR，风再起时', 'VR，风再起时', '<p><span style=\"font-size: 1em;\">图片来源@视觉中国</span><br/></p><p><br/></p><p><br/></p><blockquote><p>文丨锦缎</p></blockquote><p><br/></p><p><br/></p><p>芒格说，一生抓住少数几个投资机会就足够了。诸如我们之前讨论的新能源、物联网，都是能够持续数年乃至数十年的超级投资机会。抓住这些机遇，将是实现财富增值和阶层跃迁难得的捷径。</p><p><br/></p><p><br/></p><p>关于VR，不少人却是谈虎色变。</p><p><br/></p><p><br/></p><p>依稀记得15年暴风集团连拉29个涨停板，一时间VR概念名声大噪，席卷A股市场；16年，又被誉为VR元年，资本蜂拥而入，互联网巨子、手机寡头Facebook、谷歌、微软、腾讯、三星、Sony、苹果、华为等等纷纷开始布局，一片繁华景象，泡沫堆积……</p><p><br/></p><p><br/></p><p>不到一年光景，产品的不成熟和部分厂商恶意割韭菜行为让VR市场急转直下，泡沫破裂一片死寂，纵使扎克伯格喊出“希望10亿人使用Oculus头盔”，消费者依然选择用脚投票。</p><p><br/></p><p><br/></p><p>人们心中的成见是一座大山，在山中待久了慧眼会蒙上尘埃。所以，近期元宇宙概念的兴起和华为5G+AR峰会的召开，投资者观点两极分化严重，不少人以为又是虚伪的黄粱一梦。</p><p><br/></p><p><br/></p><p>但在认真思考后，我认为VR的需求端逻辑牢固，供给端随着行业发展已接近成熟的临界点，关键设备VR头显的价格也已经十分性感，具备大规模销售的条件，同时，几个关键节点也有望点燃市场热情，打开万亿市场空间。</p><p><br/></p><p><br/></p><p><strong>从具体投资来看，VR已经到了爆发前夜，头显与游戏将率先掀动浪潮，VR其他应用内容和AR眼镜在一段时间磨合后会逐步兴起，引领我们进入长达五十年的“元宇宙”投资盛筵。</strong></p><p><br/></p><p><br/></p><p><strong>这一行业发展过程，可简单看作从偶尔消遣的“VR游戏机”，到高依赖度的“智能手机”，最后元宇宙接棒“互联网”，扩展人类生活。</strong></p><p><br/></p><p><br/></p><p>做出这样的判断，是基于行业实际进展与逻辑推理所得，本文将以五个理由阐述。仅供交流参考，不作投资建议。</p><p><br/></p><p><br/></p><p><strong>目录：</strong></p><p><br/></p><p><br/></p><p>（一）底层逻辑：人类自然的欲望需求</p><p><br/></p><p><br/></p><p>（二）行业发展：八十年风雨，始见终章</p><p><br/></p><p><br/></p><p>（三）市场空间：每年两万三千亿？</p><p><br/></p><p><br/></p><p>（四）关键事件：解锁市场热情的钥匙</p><p><br/></p><p><br/></p><p>（五）投资维度：产业宽泛，可投资公司众多</p><p><br/></p><p><br/></p><h2>01、底层逻辑：人类自然的欲望需求</h2><p><br/></p><p><br/></p><p>三年前，斯皮尔伯格执导的《头号玩家》横空出世，备受欢迎，尤其受到Z世代追捧。电影中的虚拟宇宙“绿洲”让人心弛神往，而它实际上就是对近期火热的Metaverse元宇宙概念最贴合的演绎，即创造独立于现实世界的虚拟数字第二世界，使用户能以数字身份自由生活。</p><p><br/></p><p>为什么人们会向往“绿洲”呢？</p><p><br/></p><p><br/></p><p>这实际上与人们为什么喜欢玩游戏、看电影是一个道理，一局游戏就是一次虚拟冒险，一场电影就是一次对自身现实之外生活的参与。所以，这都源于人类最自然的欲望需求，即适时离开真实世界，追求或参与或见证其他的生活，获得更多的乐趣，满足精神的愉悦和心灵的慰藉。</p><p><br/></p><p><br/></p><p>那么，怎样的游戏能成为爆款游戏？怎样的电影让人沉醉？</p><p><br/></p><p><br/></p><p>答：<strong>强烈的沉浸感。</strong>好的游戏或电影一定可以把我们带入其中，沉浸在剧中虚拟的喜怒哀乐，不能自拔，以至于暂时忘记真实世界。</p><p><br/></p><p><br/></p><p></p><p>我们在打游戏或看电影时，视角范围在60度到120度，当我们抬头或扭头时，便回归现实，仅是部分沉浸。而“绿洲”这样的虚拟元宇宙，每个角度的视角都是360度，属于完全沉浸，感受和真实世界别无二致，这种状态一定是更吸引人的，所以元宇宙的底层需求逻辑牢不可破。</p>', 0, 1, 1584282332, 1625367794, 1);
INSERT INTO `z_post` VALUES (20, '美军不打招呼撤离阿富汗最大军事基地，几小时后抢劫者闯入袭击……', '啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦啦hello', '<p><img src=\"https://images.shobserver.com/news/690_390/2021/7/4/9abca367aa3a4b53bc6ee951cfde2e5c.jpg\"/></p><p>在美军撤离驻阿富汗最大的军事基地之后仅几个小时，数十名抢劫者就闯入该基地。</p><p><img src=\"https://images.shobserver.com/export_img/2021/7/4/02de53e4-154b-409c-99cb-755101b00386.jpg\"/></p><p>这是7月2日在阿富汗帕尔万省拍摄的美国和北约军队撤离后的巴格拉姆空军基地。新华社 图</p><p>据美联社2日报道，巴格拉姆镇的地区行政长官拉菲（darwaish raufi）表示，由于美军的离开没有与当地官员进行任何协调，基地的大门没有任何安全保障，使得抢劫者可以在阿富汗军队接管基地之前非法进入。</p><p>拉菲说：“这些抢劫者被稍后赶到的阿富汗军队拦住了，一些人被捕，其余的人已被赶出基地。”这些抢劫者在被捕前袭击了多个建筑物。</p><p>拉菲表示，目前阿富汗国防军已完全控制了基地。</p><p>阿富汗国防部副发言人法瓦德·阿曼（fawad aman）当地时间7月2日在社交媒体上发布消息称，所有外国联军和美军已于昨晚离开巴格拉姆空军基地。基地已被移交给阿富汗国防和安全部队。</p><p>海外网7月3日消息，综合《华尔街日报》、俄罗斯卫星通讯社报道，在美军撤离阿富汗之际，美国官员透露，包含美驻阿富汗使馆人员在内的数千人或离开。</p><p>报道称，出于对安全的担忧，美方加强了驻阿富汗喀布尔大使馆的撤离计划。不仅包括美国驻阿富汗的上百名使馆人员，还有留在该国的数千名美国公民都可能被撤离。然而，虽然撤离的准备工作被加快，但美方并没有当下就实施的计划。</p><p>栏目主编：赵翰露</p><p>本文作者：澎湃新闻 南博一</p><p>文字编辑：杨蓉</p><p>题图来源：新华社</p><p>图片编辑：苏唯</p>', 0, 1, 1584282332, 1625367680, 1);

-- ----------------------------
-- Table structure for z_post_tag
-- ----------------------------
DROP TABLE IF EXISTS `z_post_tag`;
CREATE TABLE `z_post_tag`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL COMMENT '文章',
  `tag_id` int(50) NOT NULL COMMENT '标签',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `post_id`(`post_id`) USING BTREE,
  INDEX `tag_id`(`tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章标签关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_post_tag
-- ----------------------------
INSERT INTO `z_post_tag` VALUES (1, 1, 1);
INSERT INTO `z_post_tag` VALUES (7, 20, 5);
INSERT INTO `z_post_tag` VALUES (8, 20, 6);
INSERT INTO `z_post_tag` VALUES (9, 18, 7);
INSERT INTO `z_post_tag` VALUES (10, 7, 8);

-- ----------------------------
-- Table structure for z_role
-- ----------------------------
DROP TABLE IF EXISTS `z_role`;
CREATE TABLE `z_role`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `permissions` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限规则ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_role
-- ----------------------------
INSERT INTO `z_role` VALUES (1, '测试', 1, '');

-- ----------------------------
-- Table structure for z_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `z_role_permission`;
CREATE TABLE `z_role_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` mediumint(8) UNSIGNED NOT NULL,
  `role_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uid_group_id`(`admin_id`, `role_id`) USING BTREE,
  INDEX `uid`(`admin_id`) USING BTREE,
  INDEX `group_id`(`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组规则表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_role_permission
-- ----------------------------
INSERT INTO `z_role_permission` VALUES (1, 1, 1);
INSERT INTO `z_role_permission` VALUES (9, 2, 1);
INSERT INTO `z_role_permission` VALUES (2, 3, 1);

-- ----------------------------
-- Table structure for z_tags
-- ----------------------------
DROP TABLE IF EXISTS `z_tags`;
CREATE TABLE `z_tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章标签' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_tags
-- ----------------------------
INSERT INTO `z_tags` VALUES (1, 'PHP');
INSERT INTO `z_tags` VALUES (3, '啦啦');
INSERT INTO `z_tags` VALUES (4, 'okok');
INSERT INTO `z_tags` VALUES (5, '美军');
INSERT INTO `z_tags` VALUES (6, '阿富汗');
INSERT INTO `z_tags` VALUES (7, 'vr');
INSERT INTO `z_tags` VALUES (8, '视频');

-- ----------------------------
-- Table structure for z_user
-- ----------------------------
DROP TABLE IF EXISTS `z_user`;
CREATE TABLE `z_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `age` tinyint(3) UNSIGNED NOT NULL DEFAULT 18 COMMENT '年龄',
  `sex` enum('1','2') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '性别',
  `status` enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '加入时间',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of z_user
-- ----------------------------
INSERT INTO `z_user` VALUES (1, '啦啦啦', '/uploads/20210619/bc7698478f5c416de6e26a96d497e09fa54ea08a.jpg', 18, '1', '1', 1624167027, '$2y$10$qaP7PQZPiv9IHj6T1E1DsewARVttPxcQJd6Mg7PaBU2JJ8hG6e/PW');

SET FOREIGN_KEY_CHECKS = 1;
